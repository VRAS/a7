
let themeBlue = '#2679ff'
let themeLightGray = '#c2c2c2'

let pickedPoint = []
let roomPointArray = []
let wallHistory = []
let wallCount = 0
let sameDirectionCount = 1
let svg = document.getElementById('svg')
let polygon = document.createElementNS('http://www.w3.org/2000/svg', 'polygon');
polygon.id = 'polygon'
svg.appendChild(polygon);


function addWall(direction){
    let wallLength = parseFloat(document.getElementById('inputLength').value)

    let lastCoordinateSet = roomPointArray.at(-1)
    let lastX=lastCoordinateSet.at(0)
    let lastY=lastCoordinateSet.at(1)

    if (wallLength !== 0 &&
        isNaN(wallLength) == false){
        wallHistory.push([wallLength, direction])
        switch(direction){
            case 'up':
                roomPointArray.push([lastX, lastY-wallLength])
                break;
            case 'down':
                roomPointArray.push([lastX, lastY+wallLength])
                break;
            case 'left':
                roomPointArray.push([lastX-wallLength, lastY])
                break;
            case 'right':
                roomPointArray.push([lastX+wallLength, lastY])
                break;
            }
        newCoordinateSet = roomPointArray.at(-1)
        if (wallCount >= 1){
            checkWalls(wallLength, wallCount, direction, newCoordinateSet)
        } else {
            wallCount += 1
            addRuler(wallLength, wallCount)
            drawRuler(wallLength, wallCount, direction, newCoordinateSet)
        // since there is no wall to compare with
        }
    addPoint()
    roomInfoUpdate()
    guidePoints('endPointCircle', newCoordinateSet)
    document.getElementById('roomInfo').style.visibility = 'visible'
    if (roomPointArray.at(0)[0] == roomPointArray.at(-1)[0] &&
        roomPointArray.at(0)[1] == roomPointArray.at(-1)[1]){
        document.getElementById('finishroom').style.visibility = 'visible'
    } else {
        document.getElementById('finishroom').style.visibility = 'hidden'
    }
    console.log(roomPointArray)
    }
}

function guidePoints(startOrEnd, coordinateSet, visibility = 'visible'){
    pointStyle = document.getElementById(startOrEnd).style
    coordinateX = coordinateSet[0]
    coordinateY = coordinateSet[1]
    pointStyle.width = pointStyle.height = 15 + 'px'
    pointStyle.visibility = visibility
    
    pointStyle.top = (coordinateY - (parseInt(pointStyle.height, 10)/2) - 2) + 'px'
    pointStyle.left = (coordinateX - (parseInt(pointStyle.width, 10)/2) - 2) + 'px'
    //since the point is placed from its top left corner, we subtract half of the height and width of the circle from its X & Y position to have it placed from the middle instead.
}

function startPoint(){
    svg.addEventListener('click', pickPoint, false)
    document.body.style.cursor = 'crosshair'
}

function pickPoint(click){
    document.body.style.cursor = 'auto'
    pointX = click.pageX
    pointY = click.pageY
    pickedPoint = [pointX, pointY]
    roomPointArray.splice(0,1,pickedPoint)
    addPoint()
    buttonstyle = document.getElementById('wallButtons').style
    buttonstyle.visibility = 'visible'
    guidePoints('startPointCircle', roomPointArray.at(0))
    document.getElementById('startpoint').style.visibility = 'hidden'
    svg.removeEventListener('click', pickPoint, false)
}

function addRuler(length, rulerNumber){
    newRuler = document.createElement('div');     //creates a variable called newDivBox, that contains instructions to create a new element in the HTML file
    newRuler.className = 'rulers';
    newRuler.id = 'ruler' + rulerNumber;
    newRuler.textContent = length;
    document.body.appendChild(newRuler);
}

let rulerSpacing = 15

function drawRuler(length, rulerNumber, direction, rulerEnd){
    rulerStyle = document.getElementById('ruler' + rulerNumber).style;
    document.getElementById('ruler' + rulerNumber).textContent = (length/100) + ' m.'
    rulerWidth = document.getElementById('ruler' + rulerNumber).offsetWidth
    rulerHeight = document.getElementById('ruler' + rulerNumber).offsetHeight

    rulerEndX = rulerEnd.at(0)
    rulerEndY = rulerEnd.at(1)

    switch(direction){
        case 'up':
            rulerStyle.transform = 'rotate(-90deg)'
            rulerStyle.top = (rulerEndY + (length/2) - (rulerHeight/2)) + 'px'
            rulerStyle.left = (rulerEndX - (rulerWidth/2) - rulerSpacing) + 'px'
        break;
        case 'down':
            rulerStyle.transform = 'rotate(90deg)'
            rulerStyle.top = (rulerEndY - (length/2) - (rulerHeight/2)) + 'px'
            rulerStyle.left = (rulerEndX - (rulerWidth/2) + rulerSpacing) + 'px'
        break;
        case 'left':
            rulerStyle.top = (rulerEndY - (rulerHeight/2) + rulerSpacing) + 'px'
            rulerStyle.left = (rulerEndX + (length/2) - (rulerWidth/2)) + 'px'
        break;
        case 'right':
            rulerStyle.top = (rulerEndY - (rulerHeight/2) - rulerSpacing) + 'px'
            rulerStyle.left = (rulerEndX - (length/2) - (rulerWidth/2)) + 'px'
        break;
    }
}

function checkWalls(wallLength, wallCounter, direction, newCoordinateSet){
prevWallDirection = wallHistory.at(-2)[1]
totalWallLength = wallLength
    if (direction == prevWallDirection){
        wallHistory.splice(-2, 2, [wallHistory.at(-2)[0]+wallLength, direction])
        //since a new "entry" was added to the history in the "addWall()" function before this function was called, we have to replace the last 2 values in the array instead of just 1.
        //we replace this entry with the entry before new latest one, and add the length of the wall to get the "full" wall length.
        totalWallLength = wallHistory.at(-1)[0]
        drawRuler(totalWallLength, wallCounter, direction, newCoordinateSet)
        polygon.points.removeItem(wallCount)
        roomPointArray.splice(-2,1)
    } else {
        wallCount += 1
        sameDirectionCount = 1
        addRuler(wallLength, wallCount)
        drawRuler(wallLength, wallCount, direction, newCoordinateSet)
    }
}

function roomInfoUpdate() {
    roomBoundingBox = polygon.getBBox()
    length = roomBoundingBox.height
    width = roomBoundingBox.width
    totalLength.textContent = "Total room length is: " + length + 'cm' 
    totalWidth.textContent = "Total room width is: " + width + 'cm'

    closingDistanceX = roomPointArray.at(0)[0] - roomPointArray.at(-1)[0]
    closingDistanceY = roomPointArray.at(0)[1] - roomPointArray.at(-1)[1]

    if (closingDistanceX > 0){
    closingDistanceXString = closingDistanceX + 'cm to the right, '
    } else if (closingDistanceX < 0) {
        closingDistanceXString = Math.abs(closingDistanceX) + 'cm to the left, '
    } else {
        closingDistanceXString = ''
    }

    if (closingDistanceY > 0){
        closingDistanceYString = closingDistanceY + 'cm downwards'
    } else if (closingDistanceY < 0) {
        closingDistanceYString = Math.abs(closingDistanceY) + 'cm upwards'
    } else {
        closingDistanceYString = ''
    }
    closingDistance.textContent = 'Distance to starting point: \r\n' + closingDistanceXString + closingDistanceYString

    if (closingDistanceX == 0 &&
        closingDistanceY == 0){
            closingDistance.textContent = 'You have returned to the start'
        }
    totalArea.textContent = 'Area of room: ' + areaFromCoords(roomPointArray)/10000 + 'm²'
}

function areaFromCoords(coordArray) {
    area = 0
    column1 = 0
    columnn2 = 0
    for (let i = 0; i<coordArray.length; i++){
        column1 += (coordArray.at(i-1)[0]*coordArray.at(i)[1])
        columnn2 += (coordArray.at(i)[0]*coordArray.at(i-1)[1])
        area = Math.abs(column1-columnn2)*0.5
        //using shoelace algorithm for calculation of area of polygon (credit to "Virtual Math" on YouTube for explaining: https://www.youtube.com/watch?v=FSWPX0XB7a0)
    }
    return (area)
}

function invertRuler(){
    rulerSpacing *= -1
    //inverts the spacing between rulers and the walls every time the function is called
    for (let i = 0; i < wallCount; i++){
        length = wallHistory.at(i)[0]
        direction = wallHistory.at(i)[1]
        pointHistory = roomPointArray.at(i+1)
        drawRuler(length, i+1, direction, pointHistory)
    }
}

function addPoint(){
    let newCoordinateSet = roomPointArray.at(-1)
    let newX=newCoordinateSet.at(0)
    let newY=newCoordinateSet.at(1)
    
        var point = svg.createSVGPoint();
        point.x = newX
        point.y = newY
        polygon.points.appendItem(point);
}

function clearWalls(){
    for (let i = wallCount; i >= 1; i--){
        undoWall()
    }
}

function undoWall(){
    if (wallCount >= 1){
        polygon.points.removeItem(wallCount)
        roomPointArray.splice(-1,1)
        wallHistory.splice(-1,1)
        document.getElementById('ruler' + wallCount).remove()
        roomInfoUpdate()
        guidePoints('endPointCircle', roomPointArray.at(-1))
        wallCount -= 1
    }
    if (wallCount == 0){
        guidePoints('startPointCircle', roomPointArray.at(0), 'hidden')
        guidePoints('endPointCircle', roomPointArray.at(-1), 'hidden')
        polygon.points.removeItem(wallCount)
        roomPointArray = []
        document.getElementById('wallButtons').style.visibility = 'hidden'
        document.getElementById('roomInfo').style.visibility = 'hidden'
        document.getElementById('startpoint').style.visibility = 'visible'
    }
    if (roomPointArray.at(0)[0] == roomPointArray.at(-1)[0] &&
    roomPointArray.at(0)[1] == roomPointArray.at(-1)[1]){
    document.getElementById('finishroom').style.visibility = 'visible'
    } else {
    document.getElementById('finishroom').style.visibility = 'hidden'
    }
}

function finishRoom(){
    document.getElementById('startPointCircle').style.visibility = 'hidden'
    document.getElementById('endPointCircle').style.visibility = 'hidden'
    document.getElementById('wallButtons').style.visibility = 'hidden'
    document.getElementById('finishroom').style.visibility = 'hidden'
    document.getElementById('wallMenu').style.visibility = 'hidden'
    wallHitboxCreator()
}

function wallHitboxCreator() {
//goes through our walls and create hitboxes(svg rects) which is used when placing doors or windows
for (let i = 0; i < wallCount; i++){
    let wallHitbox = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
    wallHitbox.id = 'wallHitbox' + (i + 1)
    wallHitbox.class = 'wallHitboxes'
    let deadspace = 10
    

    switch (wallHistory.at(i)[1]){
        case 'up':
            rectWidth = deadspace*2
            rectHeight = wallHistory.at(i)[0]  - (deadspace * 2)
            rectX = roomPointArray.at(i)[0] - deadspace
            rectY = roomPointArray.at(i)[1] - deadspace - rectHeight
        break;
        case 'down':
            rectWidth = deadspace*2
            rectHeight = wallHistory.at(i)[0]  - (deadspace * 2)
            rectX = roomPointArray.at(i)[0] - deadspace
            rectY = roomPointArray.at(i)[1] + deadspace
        break;
        case 'left':
            rectWidth = wallHistory.at(i)[0]  - (deadspace * 2)
            rectHeight = deadspace*2
            rectX = roomPointArray.at(i)[0] - deadspace - rectWidth
            rectY = roomPointArray.at(i)[1] - deadspace
        break;
        case 'right':
            rectWidth = wallHistory.at(i)[0] - (deadspace * 2)
            rectHeight = deadspace*2
            rectX = roomPointArray.at(i)[0] + deadspace
            rectY = roomPointArray.at(i)[1] - deadspace
        break;
    }
    //console.log(rectHeight, rectWidth, rectX, rectY)

    //Giving size and position to svg hitboxes
    wallHitbox.setAttribute('x', rectX);
    wallHitbox.setAttribute('y', rectY);
    wallHitbox.setAttribute('height', rectHeight);
    wallHitbox.setAttribute('width', rectWidth);

    //setting the style of hitboxes (Would not work with CSS)
    wallHitbox.style.fill = 'dodgerblue'
    wallHitbox.style.opacity = '0%'

    svg.appendChild(wallHitbox);
}
}