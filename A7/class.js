class Furniture {
    constructor(nameTag, internalName, category, furnitureWidth, furnitureDepth, price) {
        this.nameTag = nameTag
        this.internalName = internalName
        this.category = category
        this.furnitureWidth = furnitureWidth
        this.furnitureDepth = furnitureDepth
        this.price = price
        let trackMouse = this.trackMouse.bind(this)
        let hitboxOpacity = this.hitboxOpacity.bind(this)
        this.furnitureCreateHTML()
        document.addEventListener('mousemove', trackMouse, false)
        this.furnitureElement.addEventListener("click", () => 
        {document.removeEventListener('mousemove', trackMouse, false)});

        if(this.category == 'Door' || this.category == 'Window'){
        this.furnitureElement.addEventListener('click', hitboxOpacity, false)
        }
    }

    furnitureCreateHTML(){
        //creating div in HTML

        this.furnitureElement = document.createElement('button')
        this.furnitureElement.className = "furniture";
        this.furnitureElement.id = this.internalName;

        let setShift = this.setShift.bind(this)
        let rotate = this.rotate.bind(this)
        //we have to define/name the methods/functions, since otherwise the scope of "this" will get lost when theyre used as an argument when setting up the eventlistener.
        this.furnitureElement.addEventListener("mousedown", setShift);
        this.furnitureElement.addEventListener("contextmenu", rotate);

        document.body.appendChild(this.furnitureElement);
        this.furnitureElementStyle = document.getElementById(this.internalName).style
        this.furnitureElementStyle.width = this.furnitureWidth + 'px'
        this.furnitureElementStyle.height = this.furnitureDepth + 'px'
        this.furnitureElementStyle.visibility = 'hidden'
        this.furnitureElementStyle.backgroundImage = 'url(assets/furniture/' + this.category +'.png)'
    }

    trackMouse(mousemove){
        this.furnitureElementStyle.visibility = 'visible'
        this.furnitureElementStyle.left = mousemove.pageX - (this.furnitureWidth/2) + 'px'
        this.furnitureElementStyle.top = mousemove.pageY - (this.furnitureDepth/2) + 'px'

        if(this.category == 'Door' || this.category == 'Window'){
            let mousepoint = svg.createSVGPoint()
            mousepoint.x = mousemove.pageX
            mousepoint.y = mousemove.pageY
            for (let i = 0; i < wallCount; i++){
            document.getElementById('wallHitbox'+(i+1)).style.opacity = '50%'
            this.activeWallHitbox = document.getElementById('wallHitbox' + (i+1))
                if(this.activeWallHitbox.isPointInFill(mousepoint)){
                    this.trueWall = this.activeWallHitbox
                    this.wallStart = roomPointArray.at(i)
                    this.wallEnd = roomPointArray.at(i+1)
                    this.wallDirection = wallHistory.at(i)[1]

                        switch (this.wallDirection){
                            case 'up':
                            case 'down':
                                this.furnitureElementStyle.transform = 'rotate(90deg)'
                            break;
                            case 'left':
                            case 'right':
                                this.furnitureElementStyle.transform = 'rotate(0deg)'
                            break;
                        }
                    }
            }
        }
    }

    setShift(mousemove){
        this.shiftX = mousemove.pageX - parseFloat(this.furnitureElementStyle.left)
        this.shiftY = mousemove.pageY - parseFloat(this.furnitureElementStyle.top)
        //we calculate the difference/shift between from where we click on the button, and the current top(x)/left(y) placement of the top left corner of the button

        //since we have to add and remove this function from an eventlistener, we have to name the function so it knows which specific eventlistener to remove.
        //We also have to bind the function to "this" element, since sometimes when a function is used as an argument, the scope of "this." gets lost. binding the function to "this" fixes this.
        if(this.category == 'Door' || this.category == 'Window'){
            let dragDoorOrWindow = this.dragDoorOrWindow.bind(this)
            document.addEventListener('mousemove', dragDoorOrWindow, false);

            document.addEventListener("mouseup", () => 
            {document.removeEventListener('mousemove', dragDoorOrWindow, false)});
        } else {
            let drag = this.drag.bind(this)
            document.addEventListener('mousemove', drag, false);

            document.addEventListener("mouseup", () => 
            {document.removeEventListener('mousemove', drag, false)});
        }

    }
    
    drag(mousemove){
        this.cornerCalculator()
        this.oldX = this.furnitureElementStyle.left
        this.oldY = this.furnitureElementStyle.top
        this.mouseX = mousemove.pageX - this.shiftX + 'px'
        this.mouseY = mousemove.pageY - this.shiftY + 'px'
        this.oldmouseX
        this.oldmouseY
        let cornerCheck = this.cornerCollisionCheck()
        
        switch (cornerCheck){
            case 'inside':
                this.furnitureElementStyle.left = this.mouseX
                this.furnitureElementStyle.top = this.mouseY
                this.oldmouseY = mousemove.pageY
                this.oldmouseX = mousemove.pageX
                //saves the current mouse coordinates as the "old" coordinates on every frame it is inside, so that when it moves "outside", this value stops updating, and will thus be the "old" mouse coordinates before it moved outside.
            break;
            case 'top':
                this.moveDown(mousemove)
                this.furnitureElementStyle.left = this.mouseX
                this.oldmouseX = mousemove.pageX
            break;
            case 'right':
                this.moveLeft(mousemove)
                this.furnitureElementStyle.top = this.mouseY
                this.oldmouseY = mousemove.pageY
            break;
            case 'bottom':
                this.moveUp(mousemove)
                this.furnitureElementStyle.left = this.mouseX
                this.oldmouseX = mousemove.pageX
            break;
            case 'left':
                this.moveRight(mousemove)
                this.furnitureElementStyle.top = this.mouseY
                this.oldmouseY = mousemove.pageY
            break;
            case 'top left':
                this.moveDown(mousemove)
                this.moveRight(mousemove)
            break;
            case 'top right':
                this.moveDown(mousemove)
                this.moveLeft(mousemove)
            break;
            case 'bottom right':
                this.moveUp(mousemove)
                this.moveLeft(mousemove)
            break;
            case 'bottom left':
                this.moveUp(mousemove)
                this.moveRight(mousemove)
            break;
            case 'outside':
                this.furnitureElementStyle.top = this.mouseY
                this.furnitureElementStyle.left = this.mouseX
            break;
        }
    //console.log(cornerCheck)
    }

    moveUp(mousemove){
        if (mousemove.pageY < this.oldmouseY){
        //if this.mouseY is smaller than oldmouseY (can only move up)
            this.furnitureElementStyle.top = this.mouseY
        } else {
            this.furnitureElementStyle.top = this.oldY
        }
    }

    moveDown(mousemove){
        if (mousemove.pageY > this.oldmouseY){
        //if this.mouseY is bigger than oldmouseY (can only move down)
            this.furnitureElementStyle.top = this.mouseY
        } else {
            this.furnitureElementStyle.top = this.oldY
        }
    }

    moveLeft(mousemove){
        if (mousemove.pageX < this.oldmouseX){
        //if this.mouseX is smaller than oldmouseX (can only move left)
            this.furnitureElementStyle.left = this.mouseX
        } else {
            this.furnitureElementStyle.left = this.oldX
        }
    }

    moveRight(mousemove){
        if (mousemove.pageX > this.oldmouseX){
        //if this.mouseX is bigger than oldmouseX (can only move right)
            this.furnitureElementStyle.left = this.mouseX
        } else {
            this.furnitureElementStyle.left = this.oldX
        }
    }

    cornerCalculator() {
        this.boundingBox = document.getElementById(this.internalName).getBoundingClientRect()
        this.width = this.boundingBox.width
        this.depth = this.boundingBox.height
        this.x = this.boundingBox.x
        this.y = this.boundingBox.y

        this.corners = []
        this.corners[0] = [this.x, this.y] //top left corner
        this.corners[1] = [this.x + this.width, this.y] //top right corner
        this.corners[2] = [this.x + this.width, this.y + this.depth] //bottom right corner
        this.corners[3] = [this.x, this.y + this.depth] //bottom left corner

        //console.log(this.corners)

        return (this.corners)
    }

    cornerCollisionCheck(){
        this.cornerCalculator()
        let side
        let cornerCheck = []
        let point = svg.createSVGPoint()
            for (let i = 0; i < 4; i++){
                point.x = this.corners.at(i)[0]
                point.y = this.corners.at(i)[1]
                cornerCheck.push(polygon.isPointInFill(point))
                //console.log(polygon.isPointInFill(point))
            }
        //since you cant use boolean arrays in a switch statement, we have to use this long chain of "else if" statements to find out which sides of the polygon a piece of furniture is touching
        if (cornerCheck[0] && cornerCheck[1] && cornerCheck[2] && cornerCheck[3]){
        //if all points are inside
            side = 'inside'
        } else if ((cornerCheck[0] && cornerCheck[1])==false && cornerCheck[2] && cornerCheck[3]){
        //if the 2 top points are outside
            side = 'top'
        } else if (cornerCheck[0] && (cornerCheck[1] && cornerCheck[2])==false && cornerCheck[3]){
        //if the 2 right points are outside
            side = 'right'
        } else if (cornerCheck[0] && cornerCheck[1] && (cornerCheck[2] && cornerCheck[3])==false){
        //if the 2 bottom points are outside
            side = 'bottom'
        } else if (cornerCheck[0]==false && cornerCheck[1] && cornerCheck[2] && cornerCheck[3]==false){
        //if the 2 left points are outside
            side = 'left'
        } else if ((cornerCheck[0] && cornerCheck[1] && cornerCheck[2])==false && cornerCheck[3]){
        // if all points except the bottom left is outside (its in a bottom right corner)
            side = 'top right'
        } else if ((cornerCheck[0] && cornerCheck[1])==false && cornerCheck[2] && cornerCheck[3]==false){
        // if all points except the bottom right is outside (its in a top left corner)
            side = 'top left'
        } else if (cornerCheck[0] && (cornerCheck[1] && cornerCheck[2] && cornerCheck[3])==false){
        // if all points except the top left is outside (its in a bottom right corner)
            side = 'bottom right'
        } else if (cornerCheck[0]==false && cornerCheck[1] && (cornerCheck[2] && cornerCheck[3])==false){
        // if all points except the top right is outside (its in a bottom left corner)
            side = 'bottom left'
        } else if ((cornerCheck[0] && cornerCheck[1] && cornerCheck[2] && cornerCheck[3])==false)
            side = 'outside'
        //console.log(side)
        return side
    }

    hitboxOpacity(){
        for (let i = 0; i < wallCount; i++){
            document.getElementById('wallHitbox'+(i+1)).style.opacity = '0%'
        }
    }

    rotate(){
        if(this.category !== 'Door' && this.category !== 'Window'){
            this.furnitureElementStyle.transform += 'rotate(90deg)'
            this.cornerCalculator()
        }
    }

    removeFurniture(){
        document.removeChild(this.internalName)
        furnitureArrayIndex = generatedFurnitureArray.find(this.internalName)
        generatedFurnitureArray.slice(furnitureArrayIndex)
    }
//-------------------------------------DOOR & WINDOW CODE-----------------------------------------
    dragDoorOrWindow(mousemove){
        this.mouseX = mousemove.pageX - this.shiftX + 'px'
        this.mouseY = mousemove.pageY - this.shiftY + 'px'
        this.furnitureElementStyle.transformOrigin = 'top left'
        switch (this.wallDirection){
            case 'up':
                this.furnitureElementStyle.transform = 'rotate(90deg)'
                this.furnitureElementStyle.left = this.wallStart[0] + (this.furnitureDepth/2) + 'px'
                if (this.wallEnd[1] <= parseInt(this.mouseY) && parseInt(this.mouseY) <= (this.wallStart[1] - this.furnitureWidth)){
                    this.furnitureElementStyle.top = this.mouseY
                }
            break;
            case 'down':
                this.furnitureElementStyle.transform = 'scaleX(-1) rotate(90deg)'
                this.furnitureElementStyle.left = this.wallStart[0] - (this.furnitureDepth/2) + 'px'
                if (this.wallStart[1] <= parseInt(this.mouseY) && parseInt(this.mouseY) <= (this.wallEnd[1] - this.furnitureWidth)){
                    this.furnitureElementStyle.top = this.mouseY
                }
            break;
            case 'left':
                this.furnitureElementStyle.top = this.wallStart[1] - (this.furnitureDepth/2) + 'px' 
                if (this.wallEnd[0] <= parseInt(this.mouseX) && parseInt(this.mouseX) <= (this.wallStart[0] - this.furnitureWidth)){
                this.furnitureElementStyle.left = this.mouseX
                }
            break;
            case 'right':
                this.furnitureElementStyle.transformOrigin = 'center'
                this.furnitureElementStyle.transform = 'scaleY(-1)'
                this.furnitureElementStyle.top = this.wallStart[1] - (this.furnitureDepth/2) + 'px' 
                if (this.wallStart[0] <= parseInt(this.mouseX) && parseInt(this.mouseX) <= (this.wallEnd[0] - this.furnitureWidth)){
                this.furnitureElementStyle.left = this.mouseX
                }
            break;
        }

    }
}

class FurnitureButtonCreate {
    constructor(buttonNumber, category) {
        this.buttonNumber = buttonNumber
        this.positionOfButtonX = gridPositions.at(this.buttonNumber)[0]
        this.positionOfButtonY = gridPositions.at(this.buttonNumber)[1]
        this.category = category
        this.textInButton = this.category;
        this.imgSrc = category + 'Button.png'
    }


    createOneButton() {
       //creating button
        this.buttonelement = document.createElement('button')
        this.buttonelement.innerText = this.textInButton;
        
        //class is used to control attributes i want to have on all buttons, by giving them class
        this.buttonelement.className = "furnitureButtonStyle";
        
        this.buttonelement.id = this.category;

        document.body.appendChild(this.buttonelement);

        this.buttonelement.style.left = this.positionOfButtonX + "px";
        this.buttonelement.style.top = this.positionOfButtonY + "px";  
        this.buttonelement.style.backgroundImage = 'url(assets/buttons/' + this.category +'.png)'
    }
}