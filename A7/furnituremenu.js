let furnitureParameters = ['Name', 'Width', 'Depth', 'Price']

let buttonCategories = [
    ['Couch', 200, 90, 0],
    ['Plant', 25, 25, 0],
    ['Table', 130, 80, 0],
    ['Chair', 50, 50, 0],
    ['TV', 100, 10, 0],
    ['Lamp', 25, 25, 0],
    ['Rug', 200, 150, 0],
    ['Desk', 90, 60, 0],
    ['Bed', 80, 200, 0],
    ['Door', 80, 160, 0],
    ['Window', 100, 10, 0],
    ['Misc', 50, 50, 0]
]

//Organizing objects in 2d-array (with internal-names)
let furnitureInternalNameArray = []
for (let i = 0; i < buttonCategories.length; i++){
    furnitureInternalNameArray.push([])
}

let columns = 3
let rows = Math.ceil(buttonCategories.length/columns)
//calculates how many rows are needed to display buttons for every category, by dividing the amount of categoires by the amount of columns, and rounding upwards.

let buttonSpacing = 10
let buttonHeight = 90
let buttonWidth = 90
let gridPositionY = 65
let spaceDiag = 20
let gridPositions = []
let generatedFurnitureArray = []
let furnitureArray = []

let activeCategory

function calculateButtonGrid(){
    for (let i = 0; i < rows; i++){
    gridPositionX = 0
    for (let i = 0; i < columns; i++){
        gridPositions.push([gridPositionX+spaceDiag,gridPositionY+spaceDiag])
        gridPositionX += (buttonWidth + buttonSpacing)
    }
    gridPositionY += (buttonHeight + buttonSpacing)
    }
}
calculateButtonGrid()

function spawnButtons(buttonCount) {
buttons = []
    for (let i = 0; i < buttonCount; i++) {
        //parameters for FurnitureButtonCreate(buttonName, buttonNumber, category)
    buttons.push(new FurnitureButtonCreate(i, buttonCategories.at(i)[0], 0))
    buttons[i].createOneButton();
    button = document.getElementById(buttonCategories.at(i)[0])
    button.addEventListener('click', function(){furnitureMenu(buttonCategories.at(i))})
    buttonStyle = document.getElementById(buttonCategories.at(i)[0]).style
    buttonStyle.width = buttonWidth + 'px'
    buttonStyle.height = buttonHeight + 'px'
    }
};
spawnButtons(buttonCategories.length);

function furnitureMenu(buttonCategory){
    activeCategory = buttonCategory
    document.getElementById('furnitureInputContainer').style.visibility = 'visible'
    
    document.getElementById('furnitureCategory').textContent = buttonCategory[0]
    document.getElementById('furnitureName').value = ''
    document.getElementById('furnitureWidth').value = ''
    document.getElementById('furnitureDepth').value = ''
    document.getElementById('furniturePrice').value = ''

    //placeholder by category (standard measures)
    document.getElementById('furnitureCategory').textContent = buttonCategory[0]
    document.getElementById('furnitureName').placeholder = buttonCategory[0] + ' name    '
    document.getElementById('furnitureWidth').placeholder = buttonCategory[1] + ' cm'
    document.getElementById('furnitureDepth').placeholder = buttonCategory[2] + ' cm'
    document.getElementById('furniturePrice').placeholder = buttonCategory[3]
}

function cancelFurniture() {
    document.getElementById('furnitureInputContainer').style.visibility = 'hidden'
}

let internalName

function addFurniture(){

    document.getElementById('furnitureInputContainer').style.visibility = 'hidden'
    
//if input == NULL import placeholder value
    for (let i = 0; i < furnitureParameters.length; i++) {
        if (document.getElementById('furniture' + furnitureParameters[i]).value == ""){
            document.getElementById('furniture' + furnitureParameters[i]).value = activeCategory[i]
        } else {
            
        }
    }

//set furniture parameters depending on the input given in the menu
    furnitureCategory = document.getElementById('furnitureCategory').textContent
    furnitureName = document.getElementById('furnitureName').value;
    furnitureWidth = document.getElementById('furnitureWidth').value;
    furnitureDepth = document.getElementById('furnitureDepth').value;
    furniturePrice = document.getElementById('furniturePrice').value;
    
    //furnitureInternalNameArray.push(furnitureCategory + )
    //Check which furniture objects has been spawned
    //For loop der fjerne numre og tjekker maengden af items fra en category og dermed internal namer næste

//push an "internal name" into a 2d array, depending on the amount of existing furniture of the same category.
    for (let i = 0; i<buttonCategories.length; i++){
    //runs through the for loop the same amount of times as there are arrays in the "buttoncategories" array.
        if (furnitureCategory == buttonCategories.at(i)[0]){
        //if the categories match, execute the following code:
            internalName = furnitureCategory + (furnitureInternalNameArray.at(i).length)
            //generates an "internal name", by concatenating the category of the furniture, and the amount of existing furniture of that category
            furnitureInternalNameArray.at(i).push(internalName)
            //pushes the newly generated internal name into the internalNameArray
        }
    }
    //spawn object + naming
    generatedFurniture = new Furniture(furnitureName, internalName, furnitureCategory, furnitureWidth, furnitureDepth, furniturePrice)
    generatedFurnitureArray.push(generatedFurniture)
    priceTable(generatedFurnitureArray)
    checkboxGenerator()
}

let checkboxIds = []

function priceTable(furnArray) {
    let pricetext = []
    let nametext = []
    let total = 0

    for (let i = 0; i < furnArray.length; i++){
        if (furnArray[i].category == 'Door' || furnArray[i].category == 'Window' ){
            nametext.push(furnArray[i].nameTag + " (" + furnArray[i].furnitureWidth + "cm)")
            pricetext.push('')              
        } else {
            nametext.push(furnArray[i].nameTag + " (" + furnArray[i].furnitureWidth + "cm x " + furnArray[i].furnitureDepth + "cm)")
            pricetext.push(furnArray[i].price)            
        }

    }

    for (let i = 0; i < pricetext.length; i++){
        if (pricetext[i] !== ''){
            total += parseFloat(pricetext[i])
        }
    }
  
    document.getElementById('furnitureList').textContent = nametext.join('\r\n');
    document.getElementById('furniturePriceList').textContent = pricetext.join('\r\n');
    document.getElementById('totalPrice').textContent = 'Total: ' + total;
}

function checkboxGenerator(){
    checkbox = document.createElement('input')
    checkbox.type = 'checkbox'
    checkbox.disabled = false
    checkbox.class = 'checkboxes'
    checkbox.id = generatedFurniture.internalName + 'checkbox'
    checkbox.name = generatedFurniture.internalName + 'penis'
    checkboxIds.push(checkbox.id)
    document.body.appendChild(checkbox)
}

function deleteFurniture(){
checkboxCount = checkboxIds.length-1
    for (let i = checkboxCount; i >= 0; i--){
        if(document.getElementById(checkboxIds[i]).checked == true){
            //REMOVE FURNITURE
           
            document.getElementById(generatedFurnitureArray[i].internalName).remove()
            generatedFurnitureArray.splice(i, 1)
            console.log(checkboxIds[i])
            console.log(checkboxIds.length)        
    
            //REMOVE CHECKBOX
            document.getElementById(checkboxIds[i]).remove()
            console.log(checkboxIds[i])
            checkboxIds.splice(i, 1)
            console.log(checkboxIds.length)
        }
      }
    priceTable(generatedFurnitureArray)
}